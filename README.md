# :duck: Duck Coin :duck:
<img src="lib/assets/pictures/Duckcoin.png" width="420"/>




Hi! I took the opportunity to get to know Flutter better. It's version 2.81 or even higher when you are reading this.
The last time I played around with it, it was in its beta phase. So it was cool to see how many improvements they had made to it.  


<br/>
What made me interested in it was its cross-platform capabilities. Since I had done both Android(Java) and iOS(Swift) programming and today, it's even capable in web programming! So it's a 3 in 1 product. 


<br/>
While working on this project, I didn't try it on iOS products, so I didn't incorporate any Cupertino widgets. It took little time to get back rails with Flutter since I have been working with C++ mostly in my current position, but soon I got the flow going. 


<br/>


I had little time to commit to the project. So the program does the least what has requested nothing more. I would want to play around with charts and the rest of the endpoints the API had.  I would try to clean up the project and slice it up into more widgets as flutter philosophy dictates, but all of the functionality can be shown on the same screen without a problem so I conceded.


<br/>

# How to install
To get started with Flutter you need to install it from [here](https://docs.flutter.dev/get-started/install). 
There are instructions provided for many different setups.

>git clone the repository to your own computer.

Then navigate to the root of the project and run.

>Flutter pub get 

Now you should be able to run the project and start debug it.


# How it turned out.
<img src="lib/assets/pictures/screenshot.png" width="420"/><img src="lib/assets/pictures/Screenshot1.png" width="400"/>


