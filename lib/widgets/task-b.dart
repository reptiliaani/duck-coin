//This a widget used to show desired results for task B

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class TaskB extends StatefulWidget {
  final String title;
  final DateTime resultDate;
  final double resultVolume;

  const TaskB(
      {Key? key,
      required this.title,
      required this.resultDate,
      required this.resultVolume})
      : super(key: key);

  @override
  _TaskBState createState() => _TaskBState();
}

class _TaskBState extends State<TaskB> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 100,
      width: 500,
      child: Card(
          child: Column(
        children: [
          ListTile(
              title: Text(widget.title),
              subtitle: Column(
                children: [
                  const Divider(),
                  Text('DATE: ' +
                      DateFormat('yyyy-MM-dd').format(widget.resultDate)),
                  Text('VOLUME: ' +
                      widget.resultVolume.floor().toString() +
                      '€'),
                ],
              ),
              leading: const Icon(Icons.monetization_on))
        ],
      )),
    );
  }
}
