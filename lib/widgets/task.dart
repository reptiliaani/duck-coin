//This a widget used to show desired results for task A

import 'package:flutter/material.dart';

class Task extends StatefulWidget {
  final String title;
  final int result;

  const Task({
    Key? key,
    required this.title,
    required this.result,
  }) : super(key: key);

  @override
  _TaskState createState() => _TaskState();
}

class _TaskState extends State<Task> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 100,
      width: 500,
      child: Card(
          child: Column(
        children: [
          ListTile(
            title: Text(widget.title),
            subtitle: Column(
              children: [
                const Divider(),
                Text(widget.result.toString() + ' days'),
              ],
            ),
            leading: (widget.result != 0)
                ? const Icon(Icons.trending_down_outlined)
                : const Icon(Icons.trending_flat_outlined),
          )
        ],
      )),
    );
  }
}
