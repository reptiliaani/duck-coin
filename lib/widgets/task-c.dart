//This a widget used to show desired results for task C

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class TaskC extends StatefulWidget {
  final String title;
  final DateTimeRange resultRange;
  final bool wasItWorthIt;

  const TaskC({
    Key? key,
    required this.title,
    required this.resultRange,
    required this.wasItWorthIt,
  }) : super(key: key);

  @override
  _TaskCState createState() => _TaskCState();
}

class _TaskCState extends State<TaskC> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 125,
      width: 500,
      child: Card(
          child: Column(
        children: [
          ListTile(
              title: Text(widget.title),
              subtitle: widget.wasItWorthIt
                  ? Column(
                      children: [
                        const Divider(),
                        Text(
                          'BUY: ' +
                              DateFormat('yyyy-MM-dd')
                                  .format(widget.resultRange.start),
                        ),
                        Text(
                          'SELL: ' +
                              DateFormat('yyyy-MM-dd')
                                  .format(widget.resultRange.end),
                        ),
                      ],
                    )
                  : Column(
                      children: [
                        const Text('DO NOT BUY!!!!'),
                      ],
                    ),
              leading: const Icon(Icons.calendar_today))
        ],
      )),
    );
  }
}
