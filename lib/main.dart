import 'dart:convert';
import 'package:duck_coin/widgets/task-b.dart';
import 'package:duck_coin/widgets/task-c.dart';
import 'package:duck_coin/widgets/task.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart'; // Imported to format DateTimes
import 'package:http/http.dart' as http; // Imported to make API calls.

void main() {
  runApp(const MyApp());
}

// Object to store the fetched data from API.
class DataPoint {
  final DateTime date;
  final double price;
  final double volume;

  DataPoint({
    required this.date,
    required this.price,
    required this.volume,
  });
}

// Main widget my app where we initialize Material app.
// If anything would be added here it would be screenroutings and providers.
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Duck Coin',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: const MyHomePage(title: 'Duck Coin'),
    );
  }
}

// Introduction of the MyHomePage widget.
class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // Datapoints to store fetched data.
  List<DataPoint> _datapoints = [];
  // DateRange first set from yesterday to today.
  DateTimeRange selectedDateRange = DateTimeRange(
      start: DateTime.now().subtract(const Duration(days: 2)),
      end: DateTime.now());
// Overriding the widgets initialization function to make API call on start up.
// Selected range by default is today and two days before.
// This decicion was made when it was set as just today and yesterday,
// there would be instances during development where I got only one DataPoint as a response.
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((_) => _getDataPoints(
          ((selectedDateRange.start.millisecondsSinceEpoch) / 1000).floor(),
          (((selectedDateRange.end.millisecondsSinceEpoch) / 1000) + 3600)
              .floor(),
        ));
  }

  // Task A. How many days is the longest bearish (downward) trend within a given date range?
  int _longestBearishTrend = 0;

  void _getLongestBearish() {
    int counter = 0;
    for (var i = 1; i < _datapoints.length; i++) {
      // Creating a counter of to keep track how much is the iterate
      int leftToIterate = _datapoints.length;
      leftToIterate--;
      if (_datapoints.length <= 1) return;
      // Checking is there a reason to iterate rest of the list.
      if (_longestBearishTrend >= leftToIterate &&
          (counter + leftToIterate) < _longestBearishTrend) {
        setState(() {
          _longestBearishTrend = counter;
        });
      }
      if (_datapoints[i - 1].price > _datapoints[i].price) {
        counter++;
        if (i + 1 == _datapoints.length) {
          setState(() {
            _longestBearishTrend = counter;
          });
        }
      } else {
        setState(() {
          if (counter > _longestBearishTrend) {
            _longestBearishTrend = counter;
          }
        });
        counter = 0;
      }
    }
  }

  // Task B. Which date within a given date range had the highest trading volume?
  late DataPoint highestVolume;
  void _getHighestVolume() {
    DataPoint maxvolume =
        _datapoints.reduce((a, b) => a.volume > b.volume ? a : b);
    setState(() {
      highestVolume = maxvolume;
    });
  }

  // Task C Best day to Sell and Buy. Stonks can only go up.. PS. I don't like time travel....
  // BUY LOW SELL HIGH!!
  late DateTimeRange bestStonkDays;
  bool isThereBestStonkDay = true;
  void _getBestStonkDays() {
    double maxProfit = _datapoints[1].price - _datapoints[0].price;
    // Helper DataPoints.
    DataPoint bestBuyDay = _datapoints[0];
    DateTimeRange maxProfitRange =
        DateTimeRange(start: _datapoints[0].date, end: _datapoints[1].date);

    for (var i = 1; i < _datapoints.length; i++) {
      if (_datapoints[i].price - bestBuyDay.price > maxProfit) {
        maxProfit = _datapoints[i].price - bestBuyDay.price;
        maxProfitRange =
            DateTimeRange(start: bestBuyDay.date, end: _datapoints[i].date);
      }
      if (_datapoints[i].price < bestBuyDay.price) {
        bestBuyDay = _datapoints[i];
      }
    }
    setState(() {
      if (_longestBearishTrend == _datapoints.length - 1) {
        isThereBestStonkDay = false;
        bestStonkDays =
            DateTimeRange(start: _datapoints[0].date, end: _datapoints[1].date);
      } else {
        isThereBestStonkDay = true;
        bestStonkDays = maxProfitRange;
      }
    });
  }

  // Funcion to fetch the data.
  void _getDataPoints(int from, int to) async {
    // Helper lists
    final List<DataPoint> loadedDatapoints = [];
    final List<int> loadedDates = [];
    final List<double> loadedPrices = [];
    final List<double> loadedVolumes = [];

    // Fetching data
    var url = Uri.parse(
        'https://api.coingecko.com/api/v3/coins/bitcoin/market_chart/range?vs_currency=eur&from=$from&to=$to');
    final response = await http.get(url);
    final decodedResponse = json.decode(response.body) as Map<String, dynamic>;
    // Filling helperlists with the fetched data.
    decodedResponse.forEach((key, value) {
      if (key == 'prices') {
        for (var item in value) {
          loadedDates.add(item[0]);
          loadedPrices.add(item[1]);
        }
      }
      if (key == 'total_volumes') {
        for (var item in value) {
          loadedVolumes.add(item[1]);
        }
      }
    });
    // API is a liar! You shouldn't get this many responds but it does so I have to clean every respond from the API.
    // When the query is 90+ days old you still get almost hourly data.
    for (var i = 0; i < loadedDates.length; i++) {
      if (i == 0) {
        loadedDatapoints.add(DataPoint(
            date: DateTime.fromMillisecondsSinceEpoch(loadedDates[i]),
            price: loadedPrices[i],
            volume: loadedVolumes[i]));
      } else if ((DateTime.fromMillisecondsSinceEpoch(loadedDates[i - 1]).day ==
              DateTime.fromMillisecondsSinceEpoch(loadedDates[i]).day) &
          (DateTime.fromMillisecondsSinceEpoch(loadedDates[i - 1]).month ==
              DateTime.fromMillisecondsSinceEpoch(loadedDates[i]).month) &
          (DateTime.fromMillisecondsSinceEpoch(loadedDates[i - 1]).year ==
              DateTime.fromMillisecondsSinceEpoch(loadedDates[i]).year)) {
      } else {
        loadedDatapoints.add(DataPoint(
            date: DateTime.fromMillisecondsSinceEpoch(loadedDates[i]),
            price: loadedPrices[i],
            volume: loadedVolumes[i]));
      }
    }
    // Setting the fetched data to dataponts List and calling rest of the functions to get Scrooge's tasks done.
    setState(() {
      _longestBearishTrend = 0;
      _datapoints = loadedDatapoints;
      _getHighestVolume();
      _getLongestBearish();
      _getBestStonkDays();
    });
  }

  // Function where user sets the DateTimeRange.
  void _selectDateRange(BuildContext context) async {
    final DateTimeRange? picked = await showDateRangePicker(
        context: context,
        initialDateRange: selectedDateRange,
        firstDate: DateTime(2009, 1),
        lastDate: DateTime.now());
    if (picked != null && picked != selectedDateRange) {
      setState(() {
        selectedDateRange = picked;
        _getDataPoints(
          ((selectedDateRange.start.millisecondsSinceEpoch) / 1000).floor(),
          (((selectedDateRange.end.millisecondsSinceEpoch) / 1000) + 3600)
              .floor(),
        );
      });
    }
  }

  // Build function where the wigets are build.
  // Tried to make it cleaner by seperating each task card into own widgets.
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const Image(
          image: AssetImage('lib/assets/pictures/Duckcoin.png'),
        ),
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            if (_datapoints.isNotEmpty)
              Task(
                title: 'The biggest bearish trend in the given range.',
                result: _longestBearishTrend,
              ),
            if (_datapoints.isNotEmpty)
              TaskB(
                title: 'The day with the highest volume.',
                resultDate: highestVolume.date,
                resultVolume: highestVolume.volume,
              ),
            if (_datapoints.isNotEmpty && isThereBestStonkDay)
              TaskC(
                title: 'Dates when you should have bought and sold.',
                resultRange: bestStonkDays,
                wasItWorthIt: isThereBestStonkDay,
              ),
            if (!isThereBestStonkDay)
              TaskC(
                title: 'Do not buy!!!',
                resultRange: bestStonkDays,
                wasItWorthIt: isThereBestStonkDay,
              ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(DateFormat('yyyy-MM-dd').format(selectedDateRange.start)),
                const Text('  -  '),
                Text(DateFormat('yyyy-MM-dd').format(selectedDateRange.end)),
              ],
            ),
            ElevatedButton(
                onPressed: () => _selectDateRange(context),
                child: const Text('Pick the Range')),
          ],
        ),
      ),
    );
  }
}
